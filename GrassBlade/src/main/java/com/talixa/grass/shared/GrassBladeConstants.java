package com.talixa.grass.shared;

public class GrassBladeConstants {
	public static final String VERSION = "1.0";

	public static final String TITLE_MAIN = "Grass Blade Simulator";	
	public static final String TITLE_ABOUT = "About " + TITLE_MAIN;
	
	public static final int APP_WIDTH = 555;
	public static final int APP_HEIGHT = 640;
	
	public static final int BORDER = 10;
	public static final int BORDER_SMALL = 5;
	
	public static final String LABEL_OK = "Ok";	
	
	public static final String MENU_FILE = "File";	
	public static final String MENU_EXIT = "Exit";	
	public static final String MENU_OPTIONS = "Options";
	public static final String OPTION_SHOW_FRAMEWORK = "Show Framework";
	public static final String OPTION_SHOW_GRASS = "Show Grass";
	public static final String OPTION_NUM_POINTS = "Number of Strings";
	public static final String MENU_HELP = "Help";
	public static final String MENU_ABOUT = "About";
	
	public static final String ICON = "res/grass_blade.png";
}
