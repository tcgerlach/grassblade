package com.talixa.grass.shared;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

public class IconHelper {
	public static void setIcon(Window w) {
		ClassLoader cl = IconHelper.class.getClassLoader();	
		Image im = Toolkit.getDefaultToolkit().getImage(cl.getResource(GrassBladeConstants.ICON));
		w.setIconImage(im);
	}
}