package com.talixa.grass.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.talixa.grass.listeners.DefaultMouseListener;
import com.talixa.grass.listeners.DefaultMouseMotionListener;
import com.talixa.grass.math.LineHelper;

@SuppressWarnings("serial")
public class GrassPanel extends JPanel {
	
	private int segPoints = 17;					// MUST be binary number + 1
	private boolean showFramework = false;
	private boolean showGrass = true;
	
	private JFrame parent;
	
	// framework points
	private Point[] points = new Point[3];
	private int currentPoint = 0;
	private boolean doneAddingPoints = false;
	private Point dragPoint = null;	
	
	public GrassPanel(JFrame parentFrame) {
		this.parent = parentFrame;
		
		this.addMouseMotionListener(new DefaultMouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (doneAddingPoints && dragPoint != null) {
					// if we are dragging, update the drag point
					dragPoint.x = e.getPoint().x;
					dragPoint.y = e.getPoint().y;
					parent.repaint();
				}
			}
		});
		
		this.addMouseListener(new DefaultMouseListener() {									
			@Override
			public void mouseClicked(MouseEvent e) {
				// only allow three points - after that, the user must drag existing points
				if (!doneAddingPoints) {
					addPoint(e.getPoint());	
				}				
			}
			
			private static final int MAX = 10;
			
			@Override
			public void mousePressed(MouseEvent e) {
				if (doneAddingPoints) {					
					// find nearest dot - must be less than MAX away					
					double d1 = LineHelper.getDistance(points[0], e.getPoint());
					double d2 = LineHelper.getDistance(points[1], e.getPoint());
					double d3 = LineHelper.getDistance(points[2], e.getPoint());
					
					// if dot found, begin dragging point
					if (d1 < MAX && d1 < d2 && d1 < d3) {
						dragPoint = points[0];
					} else if (d2 < MAX && d2 < d1 && d2 < d3) {
						dragPoint = points[1];
					} else if (d3 < MAX && d3 < d1 && d3 < d2) {
						dragPoint = points[2];
					} else {
						dragPoint = null;
					}
				}
			}
						
		});
	}
	
	private void addPoint(Point p) {
		points[currentPoint++] = p;
		if (currentPoint == 3) {
			doneAddingPoints = true;
		}
		parent.repaint();
	}
	
	public boolean isShowFramework() {
		return showFramework;
	}
	
	public void showFramework(boolean yesNo) {
		this.showFramework = yesNo;
		parent.repaint();
	}
	
	public boolean isShowGrass() {
		return showGrass;
	}
	
	public void showGrass(boolean yesNo) {
		this.showGrass = yesNo;
		parent.repaint();
	}
	
	public void setSegmentPoints(int segPoints) {
		this.segPoints = segPoints;
		parent.repaint();
	}
	
	public int getSegmentPoints() {
		return this.segPoints;
	}
	
	@Override
	public void paint(Graphics g) {	
		super.paint(g);

		// g2 needed for grass blade width
	    Graphics2D g2 = (Graphics2D) g;
	    g2.setStroke(new BasicStroke(1));
		
		// first, clear the screen
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		
		// draw all dots that are present
		for(int i = 0 ; i < 3; ++i) {
			if (points[i] != null) {
				g.setColor(Color.ORANGE);
				g.fillOval(points[i].x-3, points[i].y-3, 6, 6);
			}
		}
		
		// if all points are present, begin processing grass blade
		Point p1 = points[0];
		Point p2 = points[1];
		Point p3 = points[2];
		if (p1 != null && p2 != null && p3 != null) {
			// draw frame from points
			if (showFramework) {
				g.setColor(Color.BLACK);
				g.drawLine(p1.x, p1.y, p2.x, p2.y);
				g.drawLine(p2.x, p2.y, p3.x, p3.y);
			}
			
			// divide frame into X equal parts
			// B is half the distance from P1 to P2
			// A is half the distance from P1 to B
			// C is half the distance from P2 to B
			// P1--A--B--C--P2
			// This process would apply to any binary number
			Point[] line1 = LineHelper.makeLineSegment(p1,p2,segPoints);
			Point[] line2 = LineHelper.makeLineSegment(p2,p3,segPoints);
			
			// draw the framework lines in gray
			if (showFramework) {
				g.setColor(Color.GRAY);
				for(int i = 1; i < line1.length-1; ++i) {
					Point a = line1[i];
					Point b = line2[i];
					g.drawLine(a.x, a.y, b.x, b.y);
				}
			}
				
			// blade of grass points will be at the intersecting points of the lines
			// for a five point line, it would be as below
			// g1 = p3, g2 = line2c, g3 = x(line c & line b), g4 = x(line b & line a), g5 = line1a, g6 = p1
			// number of total points will be one more than the number of points in the line = for a 5 part line, 6 items needed
			Point[] grass = new Point[segPoints+1];	// SEG_POINTS+1
			grass[0] = line1[0];						// 0 point is start of line1
			grass[1] = line1[1];						// 1 point is next point on line 1
			// POINTS HERE ARE FROM INTERSECTIONS
			grass[segPoints-1] = line2[segPoints-2];	// next to last point is from line 2
			grass[segPoints] = line2[segPoints-1];	// final point is end of line2
			
			// calculate in between points
			for(int i = 2; i < grass.length-2; ++i) {
				int lineIndex = i - 1;
				Point l1s = line1[lineIndex];		// start of line 1
				Point l1e = line2[lineIndex];		// end of line 1
				Point l2s = line1[lineIndex+1];		// start of line 2
				Point l2e = line2[lineIndex+1];		// end of line 2
				
				// get intersection point
				grass[i] = LineHelper.intersection(l1s, l1e, l2s, l2e);
			}
			
			// null check on grass array
			// this is necessary because it is possible that no intersection point was found
			// this is particularly common with smaller blades of grass
			// set all null points to the previous point
			for(int i = 1; i < grass.length; ++i) {
				if (grass[i] == null) {
					grass[i] = grass[i-1];
				}
			}
			
			// Draw blade of grass 
			if (showGrass) {
				g2.setStroke(new BasicStroke(3));
				g.setColor(Color.GREEN);
				for(int i = 0; i < grass.length-1; ++i) {				
					g.drawLine(grass[i].x, grass[i].y, grass[i+1].x, grass[i+1].y);
				}
			}
			
			// TODO draw perpendicular lines for width of blade
		}
	}
}
