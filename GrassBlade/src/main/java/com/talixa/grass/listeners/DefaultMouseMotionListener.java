package com.talixa.grass.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class DefaultMouseMotionListener implements MouseMotionListener {
	@Override
	public void mouseMoved(MouseEvent e) {
		// IGNORE				
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// DO NOTHING	
	}
}
