package com.talixa.grass.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DefaultMouseListener implements MouseListener{
 
	@Override
	public void mouseReleased(MouseEvent e) {
		// NOTHING
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		// NOTHING 
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		// NOTHING				
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		// NOTHING				
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// only allow three points - after that, the user must drag existing points
	}
}
