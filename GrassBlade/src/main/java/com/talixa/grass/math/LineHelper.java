package com.talixa.grass.math;

import java.awt.Point;

public class LineHelper {

	/**
	 * Create an array of points describing the segment from start to end containing 
	 * individual points equidistant from each other. 
	 * 
	 * NOTE: The size must be a power of 2 + 1 (3,5,9,17,33,etc)
	 * 
	 * @param start begin point for line
	 * @param end final point for line
	 * @param size number of points total
	 * @return array of points describing line
	 */
	public static Point[] makeLineSegment(Point start, Point end, int size) {
		Point[] line = new Point[size];
		line[0] = start; 
		line[size-1] = end;
		splitLine(line, 0, size-1);
		return line;
	}
	
	// split a line into multiple parts
	// recursive function - each recursion splits from start to end
	private static void splitLine(Point[] points, int start, int end){
		int midPoint = (start + end) / 2;
		points[midPoint] = getMidPoint(points[start], points[end]);
		if (midPoint % 2 == 0) {
			splitLine(points, start, midPoint);
			splitLine(points, midPoint, end);
		}
	}
	
	/**
	 * Calculate midpoint between two points
	 * @param p1 point 1
	 * @param p2 point 2
	 * @return midpoint
	 */
	public static Point getMidPoint(Point p1, Point p2) {
		return new Point((p1.x + p2.x)/2, (p1.y + p2.y)/2);
	}
			
	/**
	 * Return the intersection point between two line segments
	 * 
	 * @param l1s line 1 start point
	 * @param l1e line 1 end point
	 * @param l2s line 2 start point
	 * @param l2e line 2 end point
	 * @return intersection point
	 */
	public static Point intersection(Point l1s, Point l1e, Point l2s, Point l2e) {
		return intersection(l1s.x, l1s.y, l1e.x, l1e.y, l2s.x, l2s.y, l2e.x, l2e.y);
	}
	
	// determine intersection of two lines
	// points x1 and x2 define line1, points x3 and x4 define line2
	private static Point intersection(int x1,int y1,int x2,int y2, int x3, int y3, int x4,int y4) {		 
		int d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
		if (d == 0) return null;
	    int xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
	    int yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
 
		Point p = new Point(xi,yi);
		if (xi < Math.min(x1,x2) || xi > Math.max(x1,x2)) return null;
		if (xi < Math.min(x3,x4) || xi > Math.max(x3,x4)) return null;
		return p;
	}
	
	/**
	 * Calculate the distance between two points on a line
	 * 
	 * @param p1 first point
	 * @param p2 second point
	 * @return scaler value representing the distance between the two points
	 */
	public static Double getDistance(Point p1, Point p2) {
		double x2 = Math.pow((p2.x - p1.x), 2);
		double y2 = Math.pow((p2.y - p1.y), 2);
		return Math.sqrt(x2 + y2);
	}
}
