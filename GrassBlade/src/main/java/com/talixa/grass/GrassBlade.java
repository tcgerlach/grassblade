package com.talixa.grass;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.grass.frames.FrameAbout;
import com.talixa.grass.listeners.DefaultWindowListener;
import com.talixa.grass.listeners.ExitActionListener;
import com.talixa.grass.shared.GrassBladeConstants;
import com.talixa.grass.shared.IconHelper;
import com.talixa.grass.widgets.GrassPanel;

public class GrassBlade {
	
	private static JFrame frame;
	private static GrassPanel grassPanel;
	private static JCheckBoxMenuItem showFrameworkMenuItem;
	private static JCheckBoxMenuItem showGrassMenuItem;
	
	private static void createAndShowGUI() {
		frame = new JFrame(GrassBladeConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.addWindowListener(new DefaultWindowListener());
		
		grassPanel = new GrassPanel(frame);
		frame.add(grassPanel);
		
		// set icon
		IconHelper.setIcon(frame);	
													
		// add menus
		addMenus();			
		
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(GrassBladeConstants.APP_WIDTH,GrassBladeConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (GrassBladeConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (GrassBladeConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}
	
	private static void addMenus() {
		//*******************************************************************************
		// Setup file menu
		JMenu fileMenu = new JMenu(GrassBladeConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);						
			
		JMenuItem exitMenuItem = new JMenuItem(GrassBladeConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		
		fileMenu.add(exitMenuItem);	
		
		//*******************************************************************************
		// Setup options menu
		JMenu optionsMenu = new JMenu(GrassBladeConstants.MENU_OPTIONS);
		optionsMenu.setMnemonic(KeyEvent.VK_O);
		showFrameworkMenuItem = new JCheckBoxMenuItem(GrassBladeConstants.OPTION_SHOW_FRAMEWORK);
		showFrameworkMenuItem.setMnemonic(KeyEvent.VK_F);
		showFrameworkMenuItem.setSelected(false);
		showFrameworkMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				grassPanel.showFramework(showFrameworkMenuItem.isSelected());			
			}						
		});
		optionsMenu.add(showFrameworkMenuItem);
		
		showGrassMenuItem = new JCheckBoxMenuItem(GrassBladeConstants.OPTION_SHOW_GRASS);
		showGrassMenuItem.setMnemonic(KeyEvent.VK_G);
		showGrassMenuItem.setSelected(true);
		showGrassMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				grassPanel.showGrass(showGrassMenuItem.isSelected());			
			}						
		});
		optionsMenu.add(showGrassMenuItem);
		
		// option for number of segment points
		class PointListener implements ActionListener {

			private int numberPoints;
			
			public PointListener(int numberPoints) {
				this.numberPoints = numberPoints;
			}
			
			@Override
			public void actionPerformed(ActionEvent e) {				
				grassPanel.setSegmentPoints(numberPoints);
			}			
		}
		
		JMenu pointMenu = new JMenu(GrassBladeConstants.OPTION_NUM_POINTS);
		pointMenu.setMnemonic(KeyEvent.VK_S);
		ButtonGroup optionGroup = new ButtonGroup();
		for(int i = 1; i < 7; ++i) {
			String numberOfStrings = String.valueOf((int)(Math.pow(2, i)-1));
			JRadioButtonMenuItem m = new JRadioButtonMenuItem(numberOfStrings);
			if (i == 4) {
				m.setSelected(true);
			}
			int points = (int)(Math.pow(2, i) + 1);
			m.addActionListener(new PointListener(points));
			optionGroup.add(m);			
			pointMenu.add(m);
		}
		optionsMenu.add(pointMenu);
						
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(GrassBladeConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(GrassBladeConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(optionsMenu);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
